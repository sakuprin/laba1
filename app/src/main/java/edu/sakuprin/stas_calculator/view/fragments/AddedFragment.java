package edu.sakuprin.stas_calculator.view.fragments;


import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import edu.sakuprin.stas_calculator.R;
import edu.sakuprin.stas_calculator.services.impl.Calculator;

public class AddedFragment extends Fragment {

    private Calculator calculator = new Calculator();


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View inflate = inflater.inflate(R.layout.add_layout, container, false);
        View resultAddedButton = inflate.findViewById(R.id.resultAddedButton);
        resultAddedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText viewAddedTextView = (EditText) inflate.findViewById(R.id.viewAddedTextView);
                try {
                    String value = viewAddedTextView.getText().toString();
                    viewAddedTextView.setText(String.format(Locale.US, calculator.calculate(value).toString()));
                } catch (Exception e) {
                    Toast.makeText(inflater.getContext(), "Произошла ошибка " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
        return inflate;
    }

    @Override
    public void onPause() {
        super.onPause();
        EditText viewAddedTextView = (EditText) getActivity().findViewById(R.id.viewAddedTextView);
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        edit.putString(getString(R.string.MESSAGE), viewAddedTextView.getText().toString());
        edit.apply();
    }

    @Override
    public void onResume() {
        super.onResume();
        TextView resultTextView = (EditText) getActivity().findViewById(R.id.viewAddedTextView);
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        resultTextView.setText(defaultSharedPreferences.getString(getString(R.string.MESSAGE), ""));
    }
}
