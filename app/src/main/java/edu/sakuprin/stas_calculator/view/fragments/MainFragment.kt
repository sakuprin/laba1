package edu.sakuprin.stas_calculator.view.fragments

import android.app.Fragment
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import edu.sakuprin.stas_calculator.R
import edu.sakuprin.stas_calculator.services.impl.Calculator

class MainFragment : Fragment() {
    var text: String? = null
    private val calculator = Calculator();

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val onCreateView = inflater!!.inflate(R.layout.main_layout, container, false)
        val textView = onCreateView.findViewById (R.id.resultTextView)as TextView
        textView.text = text
        return onCreateView
    }

    override fun onResume() {
        super.onResume()
        val resultTextView = activity.findViewById(R.id.resultTextView) as TextView
        val defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity())
        resultTextView.setText(defaultSharedPreferences.getString(getString(R.string.MESSAGE), ""))
    }

    override fun onPause() {
        super.onPause()
        val resultTextView = activity.findViewById(R.id.resultTextView) as TextView
        val defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity())
        val edit = defaultSharedPreferences.edit()
        edit.putString(getString(R.string.MESSAGE), resultTextView.text.toString())
        edit.apply()
    }
}
