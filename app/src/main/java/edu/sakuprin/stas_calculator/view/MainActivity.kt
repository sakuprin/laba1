package edu.sakuprin.stas_calculator.view


import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import edu.sakuprin.stas_calculator.R
import edu.sakuprin.stas_calculator.services.impl.Calculator
import edu.sakuprin.stas_calculator.view.fragments.AddedFragment
import edu.sakuprin.stas_calculator.view.fragments.MainFragment

class MainActivity : Activity() {
    private val calculator = Calculator();
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_file)
        val transition = fragmentManager.beginTransaction()
        transition.add(R.id.main, MainFragment())
        transition.commit()
    }

    fun clean(view: View) {
        val textView = findViewById (R.id.resultTextView)as TextView
        textView.text = ""
    }


    fun deleteLast(view: View) {
        val textView = findViewById (R.id.resultTextView)as TextView
        val toString = textView.text.toString()
        textView.text = toString.substring(0, toString.length - 1)
    }

    fun calculate(view: View) {
        val textView = findViewById (R.id.resultTextView)as TextView
        if (textView.text.isEmpty()) return
        try {
            val calculate = calculator.calculate(textView.text.toString())
            textView.text = calculate.toString()
        } catch(e: Exception) {
            Toast.makeText(this, "Произошла ошибка ${e.message}", Toast.LENGTH_LONG).show();
        }
    }

    fun onKeyClick(view: View) {
        val textView = findViewById (R.id.resultTextView)as TextView
        textView.text = textView.text.toString() + (view as Button).text.toString()
    }

    fun previous(view: View) {
        fragmentManager.popBackStack()
    }


    fun viewFullTextEdit(view: View) {
        val transition = fragmentManager.beginTransaction()
        transition.replace(R.id.main, AddedFragment())
        transition.addToBackStack("")
        transition.commit()
    }
}
