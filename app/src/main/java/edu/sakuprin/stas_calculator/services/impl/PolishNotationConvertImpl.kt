package edu.sakuprin.stas_calculator.services.impl


import edu.sakuprin.stas_calculator.services.PolishNotationConvert
import java.util.*

class PolishNotationConvertImpl : PolishNotationConvert {
    private val operation = Arrays.asList('-', '+', '*', '/', '^', '%')

    private fun opPrior(op: Char): Byte {
        when (op) {
            '^' -> return 3
            '*', '/', '%' -> return 2
        }
        return 1
    }

    override fun convert(baseString: String): String {
        val sbStack = StringBuilder("")
        val sbOut = StringBuilder("")
        for (i in 0..baseString.length - 1) {
            val currentChar = baseString[i]
            if (isOperation(currentChar)) {
                executeOperation(sbStack, sbOut, currentChar)
            } else if ('(' == currentChar) {
                sbStack.append(currentChar)
            } else if (')' == currentChar) {
                executeEndBrace(sbStack, sbOut)
            } else {
                sbOut.append(currentChar)
            }
        }
        while (sbStack.length > 0) {
            sbOut.append(" ").append(sbStack.substring(sbStack.length - 1))
            sbStack.setLength(sbStack.length - 1)
        }
        return sbOut.toString()

    }

    private fun executeEndBrace(sbStack: StringBuilder, sbOut: StringBuilder) {
        var cTmp: Char
        cTmp = sbStack.substring(sbStack.length - 1)[0]
        while ('(' != cTmp) {
            if (sbStack.length < 1) {
                throw RuntimeException("Ошибка разбора скобок. Проверьте правильность выражения.")
            }
            sbOut.append(" ").append(cTmp)
            sbStack.setLength(sbStack.length - 1)
            cTmp = sbStack.substring(sbStack.length - 1)[0]
        }
        sbStack.setLength(sbStack.length - 1)
    }

    private fun isOperation(c: Char): Boolean {
        return operation.contains(c)

    }

    private fun executeOperation(sbStack: StringBuilder, sbOut: StringBuilder, currentChar: Char) {
        var cTmp: Char
        while (sbStack.length > 0) {
            cTmp = sbStack.substring(sbStack.length - 1)[0]
            if (isOperation(cTmp) && opPrior(currentChar) <= opPrior(cTmp)) {
                sbOut.append(" ").append(cTmp).append(" ")
                sbStack.setLength(sbStack.length - 1)
            } else {
                sbOut.append(" ")
                break
            }
        }
        sbOut.append(" ")
        sbStack.append(currentChar)
    }
}
