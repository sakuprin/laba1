package edu.sakuprin.stas_calculator.services.impl

import edu.sakuprin.stas_calculator.services.GenericCalculator
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

public class Calculator : GenericCalculator<Double, String> {
    private val polishNotationConvert = PolishNotationConvertImpl()
    private val operation = Arrays.asList('-', '+', '*', '/', '^', '%')

    private fun isOperation(c: Char): Boolean {
        return operation.contains(c)

    }

    override fun calculate(value: String): Double {
        try {
            val sIn = polishNotationConvert.convert(value)
            var tempString: String
            var dB: Double
            var dA: Double
            val stack = ArrayDeque<Double>()
            val st = StringTokenizer(sIn)
            while (st.hasMoreTokens()) {
                try {
                    tempString = st.nextToken().trim { it <= ' ' }
                    if (1 == tempString.length && isOperation(tempString[0])) {
                        if (stack.size < 2) {
                            throw RuntimeException("Неверное количество данных в стеке для операции " + tempString)
                        }
                        dB = stack.pop()
                        dA = stack.pop()
                        when (tempString[0]) {
                            '+' -> dA += dB
                            '-' -> dA -= dB
                            '/' -> dA /= dB
                            '*' -> dA *= dB
                            '%' -> dA %= dB
                            '^' -> dA = Math.pow(dA, dB)
                            else -> throw RuntimeException("Недопустимая операция " + tempString)
                        }
                        stack.push(dA)
                    } else {
                        dA = java.lang.Double.parseDouble(tempString)
                        stack.push(dA)
                    }
                } catch (e: Exception) {
                    throw RuntimeException("Недопустимый символ в выражении")
                }

            }

            if (stack.size > 1) {
                throw Exception("Количество операторов не соответствует количеству операндов")
            }

            val result = stack.pop()
            return BigDecimal(result).setScale(3, RoundingMode.UP).toDouble();
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}
