package edu.sakuprin.stas_calculator.services

interface PolishNotationConvert {
    fun convert(baseString: String): String
}
