package edu.sakuprin.stas_calculator.services


interface GenericCalculator<T, T2> {
    fun calculate(value: T2): T
}
